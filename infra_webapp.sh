#!/bin/bash

#################################################################################
#		infrastructure webapp						#
#################################################################################


nomdugroupe=Jeremy_brief_8
nomduwebplan=Jeremy-webplan
nomduwebapp=Jeremy-webapp
nommariadb=Jeremy-mariadb
usermariadb=Jeremy
pwdmariadb=Promo20cloud
nomaccountstorage=jeremyaccount
nomcontainer=jeremycontainer



group_create(){

	az group create -l northeurope -n $nomdugroupe
}

webplan_create(){

	az appservice plan create -g $nomdugroupe -n $nomduwebplan --sku P2V3 --is-linux
}

webapp_create(){

	az webapp create -g $nomdugroupe -p $nomduwebplan -n $nomduwebapp -r php:8.0}

mariadb_create(){

	az mariadb server create -l francecentral -g $nomdugroupe -n $nommariadb -u $usermariadb -p $pwdmariadb --sku-name GP_Gen5_2
}

create_storage(){

	az storage account create -g $nomdugroupe -n $nomaccountstorage
	az storage container create -n $nomcontainer --account-name $nomaccountstorage --public-access container
}

main(){

	group_create
	webplan_create
	webapp_create
	mariadb_create
	create_storage
}

main
